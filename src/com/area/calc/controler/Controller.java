
package com.area.calc.controler;

import com.area.calc.model.BaseShape;
import com.area.calc.model.Circle;
import com.area.calc.model.Rhombus;
import com.area.calc.model.Ellipse;
import com.area.calc.model.Square;
import com.area.calc.model.Triangle;
import com.area.calc.view.AreaCalc;
import com.area.calc.view.AreaCalc.ShapeType;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author mmbillah
 * It is the event handler for calculate button
 */
public class Controller {
	
	double width;
	double height;
        ShapeType selectedShapeType;
         
	public void setWidth(double width) {
		this.width = width;
	}
    

	public void setHeight(double height) {
		this.height = height;
	}

	public void setShapeType( ShapeType shapeType) {
		this.selectedShapeType = shapeType;
	}
        /**
         * 
         * @return Area of Shape
         */
        
    public double shapeCalculation()
    {
        BaseShape s=null;  //parent reference
        if(selectedShapeType == ShapeType.TRIANGL)
        {
            s = new Triangle(width,height);
        }
        else if( selectedShapeType == ShapeType.ELLIPSE)
        {
            s = new Ellipse(width,height);
        }
        else if(selectedShapeType == ShapeType.RHOMBUS)
        {
            s = new Rhombus(width,height);
        }
        else if(selectedShapeType == ShapeType.SQUARE)
        {
            s = new Square(width);
        }
        else if(selectedShapeType == ShapeType.CIRCLE)
        {
            s = new Circle(width);
        } else {
        	throw new RuntimeException("No valid shape type");
        }
        storeData(s);
        return s.getArea(); //call the overrided methode  
    }

    private void storeData(BaseShape s) {
       AreaCalc.db.add(s) ;
       
    }
    /*public List<BaseShape> getSoreData()
    {
        return db;
    }*/
    
    

}


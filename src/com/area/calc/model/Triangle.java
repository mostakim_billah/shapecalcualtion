/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.area.calc.model;

import com.area.calc.view.AreaCalc.ShapeType;

/**
 * Calculate the area of Triangle
 * @author mmbillah
 */
public class Triangle implements BaseShape {
	private double param1;
	private double param2;
        ShapeType selectedShapeType;
       /**
        * constructor for Triangle
        * @param b initialize base
        * @param h initialize height
        * 
        */
	public Triangle(double b, double h) {
		param1 = b;
		param2 = h;
                selectedShapeType = ShapeType.TRIANGL;
	}

        /**
         * 
         * @return the area of triangle
         */
    @Override
        public double getParam1()
        {
            return param1;
        }
    @Override
         public double getParam2()
        {
            return param2;
        }
    @Override
	public double getArea() {
		return 0.5 * param1 * param2;
	}
   

    @Override
    public ShapeType getShapeType() {
        return selectedShapeType;
    }
}

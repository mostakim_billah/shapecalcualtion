package com.area.calc.model;

import com.area.calc.view.AreaCalc.ShapeType;

/**
 *
 * @author mmbillah
 * calculate the area of Parallelogram
 */

public class Rhombus implements BaseShape
{
    private double param1;
    private double param2;
    private ShapeType selectedShapeType;
   
    /**
     * 
     * @param d1 initialize diagonal1
     * @param d2 initialize diagonal2
     */
    public Rhombus(double d1, double d2)
    {
        param1 = d1;
        param2 = d2;
        selectedShapeType = ShapeType.RHOMBUS;
    }
    /**
     * 
     * @return area
     */
    @Override
    public double getArea()
    {
        return 0.5*param1*param2;
    }

    @Override
    public double getParam1() {
        return param1;
    }

    @Override
    public double getParam2() {
        return param2;
    }

   

    @Override
    public ShapeType getShapeType() {
         return selectedShapeType;
    }
}
package com.area.calc.model;

import com.area.calc.view.AreaCalc.ShapeType;

/**
 * 
 * 
 * @author mmbillah
 */
public class Ellipse implements BaseShape {
	private double radius1;
	private double radius2;
         public static final double PI = 3.1415;
    private ShapeType selectedShapeType;
/**
 * constructor for rectangle
 * @param w width
 * @param h height
 */
	public Ellipse(double r1, double r2) {
		radius1 = r1;
		radius2 = r2;
                selectedShapeType = ShapeType.ELLIPSE;
	}

	/**
	 * @return area
	 */
    @Override
	public double getArea() {
		return PI*radius1 * radius2;
	}

    @Override
    public double getParam1() {
        return radius1;
    }

    @Override
    public double getParam2() {
        return radius2;
    }

   

    @Override
    public ShapeType getShapeType() {
         return selectedShapeType;
    }
}
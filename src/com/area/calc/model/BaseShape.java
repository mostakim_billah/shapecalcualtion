package com.area.calc.model;

import com.area.calc.view.AreaCalc.ShapeType;

/**
 *Interface for shape
 * @author mmbillah
 */
public interface BaseShape {
   
	/**
	 * 
	 * @return area
	 */
    public double getParam1();
    public double getParam2();
    public double getArea();
    public ShapeType getShapeType();
    
    
}

package com.area.calc.model;

import com.area.calc.view.AreaCalc.ShapeType;

/**
 *
 * @author mmbillah Calculate the area of square
 */
public class Square implements BaseShape {

    private double param1;
    private ShapeType selectedShapeType;

    /**
     * constructor for Square
     *
     * @param l initialize length
     */
    public Square(double l) {
        param1 = l;
        selectedShapeType = ShapeType.SQUARE;
    }

    @Override
    public double getParam1() {
        return param1;
    }

    /**
     *
     * @return area of square
     */
    @Override
    public double getArea() {
        return param1 * param1;
    }

    @Override
    public ShapeType getShapeType() {
        return selectedShapeType;
    }

    @Override
    public double getParam2() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    
}

package com.area.calc.model;

import com.area.calc.view.AreaCalc.ShapeType;

/**
 * 
 * @author mmbillah
 * calculate the area of circle
 */

public class Circle implements BaseShape {
	private double param1;
	final double PI = 3.1415;
    private ShapeType selectedShapeType;

        /**
         * constructor for circle
         * @param r initialize radius
         */
	public Circle(double r) {
		param1 = r;
                selectedShapeType = ShapeType.CIRCLE;
	}

        /**
         * 
         * @return the area of circle
         */
    @Override
	public double getArea() {
		return PI * param1 * param1;
	}

    @Override
    public double getParam1() {
        return param1;
    }

    @Override
    public double getParam2() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

   

    @Override
    public ShapeType getShapeType() {
        return selectedShapeType;
    }
}

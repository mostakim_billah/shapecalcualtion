/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.area.calc.view;

/**
 *
 * @author mmbillah Draw the all GUI component
 */
import com.area.calc.controler.Controller;
import com.area.calc.lib.ShapeAreaComparator;
import com.area.calc.model.BaseShape;
//import com.area.calc.model.Datastore;
//import com.area.calc.model.ShapeData;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

public class AreaCalc {

    private JFrame frmShapeCalculation;
    private JTextField widthTextBox;
    private JTextField heightTextBox;
    private JTextField areaTextBox;
    private JTable shapeDataTable;
    private JLabel shapeTypeLabel;
    private JLabel widthLabel;
    private JLabel heightLabel;
    private JLabel areaLabel;
    private JButton calculateAreaButton;
    private JLabel listOfShapesLabel;
    public JComboBox shapeListComboBox;
    public static List<BaseShape> db; //this variable contain all calculaed shape

    public enum ShapeType {

        TRIANGL, ELLIPSE, SQUARE, RHOMBUS, CIRCLE
    };
    ShapeType selectedShape;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    AreaCalc window = new AreaCalc();
                    window.frmShapeCalculation.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public AreaCalc() {
        db = new ArrayList();
        selectedShape = ShapeType.TRIANGL; //Initial Selection
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    /**
     * @return value
     */
    private void initialize() {
        frmShapeCalculation = new JFrame();
        frmShapeCalculation.setTitle("Shape Calculation");
        frmShapeCalculation.setBounds(100, 100, 481, 657);
        frmShapeCalculation.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmShapeCalculation.getContentPane().setLayout(null);

        shapeListComboBox = new JComboBox(ShapeType.values());
        shapeListComboBox.addItemListener(new ItemListener() {
            // handle JComboBox event
            @Override
            public void itemStateChanged(ItemEvent event) {
                // determine whether item selected
                if (event.getStateChange() == ItemEvent.SELECTED) {
                    heightTextBox.setText("");
                    widthTextBox.setText("");
                    areaTextBox.setText("");
                    heightTextBox.setVisible(true);
                    heightLabel.setVisible(true);
                    selectedShape = (ShapeType) shapeListComboBox.getSelectedItem(); //Selected Shape from the drop down box

                    switch (selectedShape) {
                        case TRIANGL:
                            widthLabel.setText("Base");
                            heightLabel.setText("Height");
                            break;
                        case ELLIPSE:
                            widthLabel.setText("Radius1");
                            heightLabel.setText("Radius2");
                            break;
                        case SQUARE:
                            widthLabel.setText("Length");
                            heightLabel.setVisible(false);
                            heightTextBox.setVisible(false);
                            break;
                        case RHOMBUS:
                            widthLabel.setText("Diagonal1");
                            heightLabel.setText("Diagonal2");
                            break;
                        case CIRCLE:
                            widthLabel.setText("Radius");
                            heightLabel.setVisible(false);
                            heightTextBox.setVisible(false);
                            break;
                        default:
                            JOptionPane.showMessageDialog(frmShapeCalculation, "Invalid Polygon Type");
                            break;
                    }



                }
            } // end method itemStateChanged
        });
        shapeListComboBox.setBounds(174, 13, 175, 38);
        frmShapeCalculation.getContentPane().add(shapeListComboBox);

        shapeTypeLabel = new JLabel("Type");
        shapeTypeLabel.setBounds(12, 13, 134, 38);
        frmShapeCalculation.getContentPane().add(shapeTypeLabel);

        widthLabel = new JLabel("Base");
        widthLabel.setBounds(12, 64, 134, 38);
        frmShapeCalculation.getContentPane().add(widthLabel);

        heightLabel = new JLabel("Height");
        heightLabel.setBounds(12, 115, 134, 38);
        frmShapeCalculation.getContentPane().add(heightLabel);

        widthTextBox = new JTextField();
        widthTextBox.setBounds(174, 64, 175, 38);
        frmShapeCalculation.getContentPane().add(widthTextBox);
        widthTextBox.setColumns(10);

        heightTextBox = new JTextField();
        heightTextBox.setBounds(174, 123, 175, 38);
        frmShapeCalculation.getContentPane().add(heightTextBox);
        heightTextBox.setColumns(10);

        areaLabel = new JLabel("Area");
        areaLabel.setBounds(12, 233, 134, 38);
        frmShapeCalculation.getContentPane().add(areaLabel);

        areaTextBox = new JTextField();
        areaTextBox.setEditable(false);
        areaTextBox.setBounds(174, 233, 175, 38);
        frmShapeCalculation.getContentPane().add(areaTextBox);
        areaTextBox.setColumns(10);

        calculateAreaButton = new JButton("Calculate");
        calculateAreaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (selectedShape == ShapeType.CIRCLE || selectedShape == ShapeType.SQUARE) //Only one input
                {
                    if (widthTextBox.getText().isEmpty()) //No data given
                    {
                        JOptionPane.showMessageDialog(frmShapeCalculation, "Please Enter the value");
                        return;
                    }
                } else {
                    if (widthTextBox.getText().isEmpty() || heightTextBox.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(frmShapeCalculation, "Please Enter the value");
                        return;
                    }
                }
                Controller controller = new Controller();
                double width = 1;
                try {
                    width = Double.parseDouble(widthTextBox.getText()
                            .toString());
                } catch (NumberFormatException e2) {
                    width = 0;
                }
                double height = 1;
                try {
                    height = Double.parseDouble(heightTextBox.getText()
                            .toString());
                } catch (NumberFormatException e1) {
                    height = 0;
                }
                controller.setWidth(width);
                controller.setHeight(height);
                controller.setShapeType(selectedShape);
                String result = "" + controller.shapeCalculation();
                areaTextBox.setText(result);
                listOfShapeViewUpdater();
            }
        });
        calculateAreaButton.setBounds(174, 182, 175, 38);
        frmShapeCalculation.getContentPane().add(calculateAreaButton);

        listOfShapesLabel = new JLabel("List of Shapes");
        listOfShapesLabel.setBounds(12, 307, 134, 38);
        frmShapeCalculation.getContentPane().add(listOfShapesLabel);

        shapeDataTable = new JTable();
        shapeDataTable.setModel(
                new DefaultTableModel(
                new Object[][]{
                    {null}, {null}, {null}, {null}, {null}, {null},
                    {null}, {null}, {null}, {null}, {null}, {null},
                    {null}, {null}, {null}, {null},},
                new String[]{"New column"}) {
                    @Override
                    public boolean isCellEditable(int row, int column) {
                        return false;
                    }
                });
        shapeDataTable.setBounds(12, 358, 288, 241);
        shapeDataTable.setCellSelectionEnabled(true);
        shapeDataTable.addKeyListener(new KeyAdapter() {         //Key listner to List of Shape data table
            @Override
            public void keyPressed(KeyEvent e) {
                char key = e.getKeyChar();
                int selectedColumn = shapeDataTable.getSelectedColumn();
                int selectedRow = shapeDataTable.getSelectedRow();

                if (key == 127)//pressed delete button
                {

                    try {
                        shapeDataTable.getModel().setValueAt("", selectedRow, selectedColumn); //Replace the value of cell by empty space
                        // List<ShapeData> db = Datastore.db;
                        db.remove(selectedRow);
                        listOfShapeViewUpdater();
                    } catch (IndexOutOfBoundsException e1) {
                        JOptionPane.showMessageDialog(frmShapeCalculation, "No Item Found");
                    }

                }

            }
        });
        frmShapeCalculation.getContentPane().add(shapeDataTable);
    }

    /**
     * this method draw the shape data on the window
     */
    protected void listOfShapeViewUpdater() {


        Collections.sort(db, new ShapeAreaComparator()); //Sorting the shape data 
        Iterator<BaseShape> iteratorDb = db.iterator();
        int i = 0;
        for (int j = 0; j < shapeDataTable.getRowCount(); j++) {
            shapeDataTable.getModel().setValueAt("", j, 0);
            // shapeDataTable.;
        }
        while (iteratorDb.hasNext()) {
            BaseShape dbElement = iteratorDb.next();
            if (dbElement.getShapeType() == ShapeType.CIRCLE || dbElement.getShapeType() == ShapeType.SQUARE) {

                this.shapeDataTable.getModel().setValueAt(
                        dbElement.getShapeType() + "( " + dbElement.getParam1()
                        + " )", i, 0);
            } else {
                this.shapeDataTable.getModel().setValueAt(
                        dbElement.getShapeType() + "( " + dbElement.getParam1()
                        + " , " + dbElement.getParam2() + " )", i, 0);
            }
            i++;
        }
        this.shapeDataTable.repaint();
    }
}

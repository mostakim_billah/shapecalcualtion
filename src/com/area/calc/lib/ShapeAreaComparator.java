package com.area.calc.lib;

import com.area.calc.model.BaseShape;
import java.util.Comparator;
/**
 * 
 * @author mmbillah
 * Sort the shape data According to the ascending order of shape area
 */
public class ShapeAreaComparator implements Comparator<BaseShape> {

	
	public int compare(BaseShape o1, BaseShape o2) {
		return (o1.getArea() < o2.getArea() ? -1 : (o1.getArea() == o2
				.getArea() ? 0 : 1));
	}

}
